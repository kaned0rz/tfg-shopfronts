<?php
namespace Acme\HelloBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Acme\HelloBundle\Entity\Product;

class HelloController extends Controller
{
    public function indexAction($name)
    {
        //return new Response('<html><body>Hello '.$name.'!</body></html>');
        //return $this->render('AcmeHelloBundle:Hello:index.html.twig', array('name' => $name));
        $product = new Product();
	    $product->setName('A Foo Bar');
	    $product->setPrice('19.99');
	    $product->setDescription('Lorem ipsum dolor');
	 
	    $em = $this->getDoctrine()->getManager();
	    $em->persist($product);
	    $em->flush();
	 
	    return new Response('Created product id '.$product->getId());
    }
}