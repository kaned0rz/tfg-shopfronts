<?php

namespace Acme\ShopfrontsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller {
    public function indexAction() {
        $user = $this->get('security.context')->getToken()->getUser();
        $title = 'Hola admin ' . $user->getUsername();
        return $this->render('AcmeShopfrontsBundle:Admin:index.html.twig', array('page_title' => $title,'user' => $user));
    }
}
