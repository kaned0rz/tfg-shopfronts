<?php

namespace Acme\ShopfrontsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Acme\ShopfrontsBundle\Entity\Shopfront;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Acme\ShopfrontsBundle\Entity\User;
use Acme\ShopfrontsBundle\Entity\Role;
use Acme\ShopfrontsBundle\Model\dao\ProductDAO;

class DefaultController extends Controller {

    public function indexAction() {
        //$repository = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Shopfront');
        // obtiene todos los productos
        //$shopfronts = $repository->findAll();
        //$dao = new ProductDAO();
        $dao = $this->get('ProductDAO');
        return $this->render('AcmeShopfrontsBundle:Default:index.html.twig', array('page_title' => 'Bienvenido a Shopfronts'));
        //return new Response('<html><body>Hello shopfronts! total: '.count($shopfronts).'</body></html>');
    }

    public function createAction() {
        //return $this->render('AcmeShopfrontsBundle:Default:index.html.twig', array('name' => 'shopfronts'));
        //return new Response('<html><body>Hello shopfronts!</body></html>');

        $shopfront = new Shopfront();
        $shopfront->setName('Shopfront X');
        $shopfront->setDescription('Descripcion Shopfront X');

        $em = $this->getDoctrine()->getManager();
        $em->persist($shopfront);
        $em->flush();

        return new Response('Created shopfront id ' . $shopfront->getId());
    }

    public function getAction($id) {
        $shopfront = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Shopfront')->find($id);
        return $this->render('AcmeShopfrontsBundle:Shopfronts:shopfront.html.twig', array('page_title' => 'Shopfront', 'shopfront' => $shopfront));
        //return new Response('<html><body>Hello shopfronts! total: '.count($shopfronts).'</body></html>');
    }

    public function loginAction(Request $request) {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
                        'AcmeShopfrontsBundle:Default:login.html.twig', array(
                    // last username entered by the user
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error,
                    'page_title' => 'Login page'
                        )
        );
    }

    public function createUserAction() {

        $user = new User();

        $user->setUsername('user@user.com');
        $user->setEmail('user@user.com');
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $user->setPassword($encoder->encodePassword('user', $user->getSalt()));
        $role = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Role')->find(2);
        $user->addRole($role);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($user);
        $em->flush();

        return new Response('Sucessful');
    }
    
    public function createAdminAction() {

        $user = new User();

        $user->setUsername('admin@admin.com');
        $user->setEmail('admin@admin.com');
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $user->setPassword($encoder->encodePassword('admin', $user->getSalt()));
        $role = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Role')->find(1);
        $user->addRole($role);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($user);
        $em->flush();

        return new Response('Sucessful');
    }

}
