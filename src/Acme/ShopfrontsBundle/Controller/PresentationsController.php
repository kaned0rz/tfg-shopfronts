<?php

namespace Acme\ShopfrontsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Doctrine\Common\Collections\Criteria;

use Acme\ShopfrontsBundle\Entity\Presentation;
use Acme\ShopfrontsBundle\Entity\Product;
class PresentationsController extends Controller
{
    public function indexAction()
    {
    	$repository = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Presentation');
    	// obtiene todos los productos
		$presentations = $repository->findAll();
        return $this->render('AcmeShopfrontsBundle:Presentations:presentations.html.twig', array('page_title' => 'Presentations','presentations' => $presentations));
    }

    public function createAction()
    {
        $presentation = new Presentation();
	    $presentation->setName('Presentacion X');
	    $presentation->setDescription('Descripcion Presentacion X');
	 
	    $em = $this->getDoctrine()->getManager();
	    $em->persist($presentation);
	    $em->flush();
	 
	    return new Response('Created presentation id '.$presentation->getId());
    }

    public function getAction($id)
    {
    	$presentation = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Presentation')->find($id);

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('SELECT p FROM Acme\ShopfrontsBundle\Entity\Product p WHERE :presentation NOT MEMBER OF p.presentations');
        $query->setParameter('presentation', $presentation);
        $products = $query->getResult();

        return $this->render('AcmeShopfrontsBundle:Presentations:presentation.html.twig', array('page_title' => 'Presentation','presentation' => $presentation,
         'products' => $products));
    }

    public function addProductAction($id,$id_product)
    {
        $presentation = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Presentation')->find($id);
        $product = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:product')->find($id_product);
        // relaciona este producto con una categoría
        $presentation->addProduct($product);
 
        $em = $this->getDoctrine()->getManager();
        $em->persist($presentation);
        $em->flush();

        return $this->redirect($this->generateUrl('acme_presentations_concrete',array('id' => $id)));
    }

    public function playAction($id){
        $presentation = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Presentation')->find($id);
        $products = $presentation->getProducts();
        /*return $this->render('AcmeShopfrontsBundle:Presentations:presentacion1.html.twig', array('page_title' => 'Presentation','presentation' => $presentation,
            'products' => $products));*/
        //return $this->render('@AcmeShopfrontsBundle/Resources/public/templates/1/index.html.twig',array('products' => $products));
        $template_id = 1;
        return $this->render('@templates/'.$template_id.'/index.html.twig',array('products' => $products,'template_id' => $template_id));
    }
}