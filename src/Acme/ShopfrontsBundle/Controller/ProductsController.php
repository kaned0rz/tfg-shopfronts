<?php

namespace Acme\ShopfrontsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Acme\ShopfrontsBundle\Entity\Product;

class ProductsController extends Controller {

    public function indexAction() {
        $repository = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Product');
        // obtiene todos los productos
        $products = $repository->findAll();
        //$categories = $this->get('security.context')->getToken()->getUser()->getCategories();
        $categories = $this->getUser()->getCategories();
        return $this->render('AcmeShopfrontsBundle:Products:products.html.twig', array('page_title' => 'Productos', 'products' => $products,'categories' => $categories));
    }

    public function createAction() {
        $product = new Product();
        $product->setName('Producto X');
        $product->setDescription('Descripcion Producto X');

        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        return new Response('Created product id ' . $product->getId());
    }

    public function getAction($id) {
        $product = $this->getDoctrine()->getRepository('AcmeShopfrontsBundle:Product')->find($id);
        return $this->render('AcmeShopfrontsBundle:Products:product.html.twig', array('page_title' => 'Presentation', 'product' => $product));
    }

}
