<?php

namespace Acme\ShopfrontsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Acme\ShopfrontsBundle\Entity\User;

class UserController extends Controller {
    public function indexAction() {
        $user = $this->get('security.context')->getToken()->getUser();
        $title = 'Hola ' . $user->getUsername();
        return $this->render('AcmeShopfrontsBundle:User:index.html.twig', array('page_title' => $title,'user' => $user));
    }
}
