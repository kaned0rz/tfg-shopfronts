<?php

namespace Acme\ShopfrontsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
 
/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
 
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;
 
    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="Presentation", mappedBy="products")
     **/
    private $presentations;
    
    /**
     * @ORM\ManyToMany(targetEntity="Category", mappedBy="products")
     **/
    private $categories;

    public function __construct() {
        $this->presentations = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add presentations
     *
     * @param \Acme\ShopfrontsBundle\Entity\Presentation $presentations
     * @return Product
     */
    public function addPresentation(\Acme\ShopfrontsBundle\Entity\Presentation $presentations)
    {
        $this->presentations[] = $presentations;
    
        return $this;
    }

    /**
     * Remove presentations
     *
     * @param \Acme\ShopfrontsBundle\Entity\Presentation $presentations
     */
    public function removePresentation(\Acme\ShopfrontsBundle\Entity\Presentation $presentations)
    {
        $this->presentations->removeElement($presentations);
    }

    /**
     * Get presentations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresentations()
    {
        return $this->presentations;
    }

    /**
     * Add categories
     *
     * @param \Acme\ShopfrontsBundle\Entity\Category $categories
     * @return Product
     */
    public function addCategory(\Acme\ShopfrontsBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Acme\ShopfrontsBundle\Entity\Category $categories
     */
    public function removeCategory(\Acme\ShopfrontsBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
