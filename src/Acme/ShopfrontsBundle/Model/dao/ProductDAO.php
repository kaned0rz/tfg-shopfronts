<?php

namespace Acme\ShopfrontsBundle\Model\dao;

use Doctrine\ORM\EntityManager;
use Acme\ShopfrontsBundle\Entity\Product;

/**
 * Description of productdao
 *
 * @author kaned0rz
 */
class ProductDAO {
    
    private $em;
    public function __construct(EntityManager $em)
    {
      $this->em = $em;
    }

    public function doInsertar(Product $product) {
        //$repository = $this->em->getRepository('AcmeShopfrontsBundle:Product');
        // obtiene todos los productos
        //$products = $repository->findAll();
        
        //$em = $this->getDoctrine()->getManager();
        $this->em->persist($product);
        $this->em->flush();
        echo "llego";
    }
}
