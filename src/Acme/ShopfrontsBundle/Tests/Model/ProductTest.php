<?php
// src/Acme/DemoBundle/Tests/Utility/CalculatorTest.php

namespace Acme\ShopfrontsBundle\Tests\Entity;

require_once(__DIR__ . "/../../../../../app/AppKernel.php");

 
//use Acme\ShopfrontsBundle\Model\dao\ProductDAO;
use Acme\ShopfrontsBundle\Entity\Product;
 
class ProductTest extends \PHPUnit_Framework_TestCase 
{
    protected $_application;
    protected $_container;
    
    public function getContainer()
    {
      return $this->_application->getKernel()->getContainer();
    }
    
    public function setUp()
    {
      $kernel = new \AppKernel("test", true);
      $kernel->boot();
      $this->_application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
      $this->_application->setAutoExit(false);
      $this->_container = $this->_application->getKernel()->getContainer();
      /*$this->runConsole("doctrine:schema:drop", array("--force" => true));
      $this->runConsole("doctrine:schema:create");
      $this->runConsole("doctrine:fixtures:load", array("--fixtures" => __DIR__ . "/../DataFixtures"));*/
    }
    
    protected static $dao;

    public static function setUpBeforeClass()
    {
        //self::$dao = $this->get('ProductDAO');
    }
    
    public static function tearDownAfterClass()
    {
        //self::$dao = NULL;
    }
    
    
    public function testAdd()
    {
        //self::$dao = new ProductDAO();
        //self::$dao = $this->get('ProductDAO');
        $dao = $this->_container->get('ProductDAO');
        
        $product = new Product();
        $product->setName("Nombre prueba");
        $product->setDescription("Descripcion prueba");
        
        $dao->doInsertar($product);
        
        // ¡acierta que nuestra calculadora suma dos números correctamente!
        $this->assertEquals(2, 1);
    }
    
}

